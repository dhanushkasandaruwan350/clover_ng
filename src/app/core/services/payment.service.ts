import {Injectable} from '@angular/core';
import {ROUTES} from "../helpers/API";
import {HttpClient} from "@angular/common/http";
import {RequestHandler} from "../helpers/RequestHandler";
import {Observable} from "rxjs";

const PAYMENT_API = ROUTES.ENDPOINTS.PAYMENT;

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private http: HttpClient, private requestHandler: RequestHandler) {
  }

  _create_card_token(data: any): Observable<any> {
    return this.requestHandler.request(PAYMENT_API.CREATE_CARD_TOKEN, data);
  }

}
