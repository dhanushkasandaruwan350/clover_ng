import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {RequestHandler} from "../helpers/RequestHandler";
import {Observable} from "rxjs";
import {ROUTES} from "../helpers/API";

const CATEGORY_API = ROUTES.ENDPOINTS.CATEGORY;

@Injectable({
  providedIn: 'root'
})


export class CategoryService {

  constructor(private http: HttpClient, private requestHandler: RequestHandler) {
  }

  _all_category(): Observable<any> {
    return this.requestHandler.request(CATEGORY_API.ALL);
  }
}
