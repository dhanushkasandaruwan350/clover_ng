import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {RequestHandler} from "../helpers/RequestHandler";
import {Observable} from "rxjs";
import {ROUTES} from "../helpers/API";

const ITEM_API = ROUTES.ENDPOINTS.ITEMS;

@Injectable({
  providedIn: 'root'
})


export class ItemService {

  constructor(private http: HttpClient, private requestHandler: RequestHandler) {
  }

  _all_items(): Observable<any> {
    return this.requestHandler.request(ITEM_API.ALL);
  }

  _all_items_stock(): Observable<any> {
    return this.requestHandler.request(ITEM_API.ALL_STOCK);
  }
}
