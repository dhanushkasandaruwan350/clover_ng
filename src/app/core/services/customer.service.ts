import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {RequestHandler} from "../helpers/RequestHandler";
import {Observable} from "rxjs";
import {ROUTES} from "../helpers/API";

const CUSTOMER_API = ROUTES.ENDPOINTS.CUSTOMERS;

@Injectable({
  providedIn: 'root'
})


export class CustomerService {

  constructor(private http: HttpClient, private requestHandler: RequestHandler) {
  }

  _get_customer(cid: any): Observable<any> {
    return this.requestHandler.request(CUSTOMER_API.GET_CUSTOMER, cid);
  }

  _create_customer(data: any): Observable<any> {
    return this.requestHandler.request(CUSTOMER_API.CREATE_CUSTOMER, data);
  }
}
