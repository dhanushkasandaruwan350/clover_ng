import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RequestHandler {

  constructor(private http: HttpClient) {
  }

  public request(conf: any, data?: any): Observable<any> {
    if (!data) {
      data = {};
    }
    if (conf.TYPE === 'POST') {
      return this.http.post(conf.URL, data).pipe(
        map((t: any) => {
          return t;
        })
        , catchError(error => {
          console.log(error);
          return throwError('Request failed');
        }));
    } else {
      return this.http.get(conf.URL, {
        params: Object.entries(data).reduce(
          (params, [key, value]) => params.set(key, '' + value), new HttpParams())
      }).pipe(
        map((t: any) => {
          return t;
        })
        , catchError(error => {
          console.log(error);
          return throwError('Request failed');
        }));
    }
  }
}
