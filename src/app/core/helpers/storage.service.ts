import {Inject, Injectable} from '@angular/core';
import {LOCAL_STORAGE, StorageService} from 'ngx-webstorage-service';
import {STORAGE_KEYS} from "./STORAGE_KEYS";
import {BehaviorSubject} from "rxjs";

@Injectable()
export class LocalStorageService {
  public cart = new BehaviorSubject(null);

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {
  }

  public storeOnLocalStorage(STORAGE_KEY: string, DATA: any): void {
    this.storage.set(STORAGE_KEY, DATA);
  }

  public storeCartItems(DATA: any): void {
    const currentItemList = this.storage.get(STORAGE_KEYS.CART_ITEMS) || [];
    currentItemList.push(DATA);
    this.storage.set(STORAGE_KEYS.CART_ITEMS, currentItemList);
    this.cart.next(this.storage.get(STORAGE_KEYS.CART_ITEMS));
  }

  public checkoutProducts(DATA: any): void {
    this.storage.set(STORAGE_KEYS.CART_ITEMS, DATA);
  }

  public removeItem(DATA: any): void {
    this.storage.remove(DATA);
  }

  public retrievesStoreItems(KEY: any) {
    return this.storage.get(KEY);
  }
}
