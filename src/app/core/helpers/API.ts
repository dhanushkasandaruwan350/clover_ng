export class ROUTES {
  public static HTTP_PREFIX = 'https://sandbox.dev.clover.com/v3/merchants/88M9MWYTSCH51/';
  public static ENDPOINTS = {
    CATEGORY: {
      ALL: {
        URL: ROUTES.HTTP_PREFIX + 'categories',
        TYPE: 'GET'
      }
    },
    ITEMS: {
      ALL: {
        URL: ROUTES.HTTP_PREFIX + 'items',
        TYPE: 'GET'
      },
      ALL_STOCK: {
        URL: ROUTES.HTTP_PREFIX + 'item_stocks',
        TYPE: 'GET'
      }
    },
    CUSTOMERS: {
      GET_CUSTOMER: {
        URL: ROUTES.HTTP_PREFIX + 'customers',
        TYPE: 'GET'
      },
      CREATE_CUSTOMER: {
        URL: 'https://scl-sandbox.dev.clover.com/v1/customers',
        TYPE: 'POST'
      }
    },
    PAYMENT: {
      PAKMS: {
        URL: 'https://apisandbox.dev.clover.com/pakms/apikey',
        TYPE: 'GET'
      },
      CREATE_CARD_TOKEN: {
        URL: 'https://token-sandbox.dev.clover.com/v1/tokens',
        TYPE: 'POST'
      }
    }
  }
}
