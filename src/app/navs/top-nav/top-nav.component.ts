import {Component, OnInit} from '@angular/core';
import {CategoryService} from "../../core/services/category.service";
import {Observable} from "rxjs";
import {StorageService} from "ngx-webstorage-service";
import {LocalStorageService} from "../../core/helpers/storage.service";
import {STORAGE_KEYS} from "../../core/helpers/STORAGE_KEYS";

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit {

  categories: any;
  cartItems: any;

  constructor(private categoryService: CategoryService, private storageService: LocalStorageService) {
    this.storageService.cart.subscribe((value: any) => {
      this.cartItems = value;
    });
  }

  ngOnInit(): void {
    this._load_all_category();
    this._check_cart();
  }

  private _load_all_category() {
    this.categoryService._all_category().subscribe(res => {
      this.categories = res.elements;
      console.log(this.categories);
    });
  }

  private _check_cart() {
    this.cartItems = this.storageService.retrievesStoreItems(STORAGE_KEYS.CART_ITEMS) || undefined;
  }
}
