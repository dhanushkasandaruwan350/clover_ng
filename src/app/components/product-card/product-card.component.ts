import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LocalStorageService} from "../../core/helpers/storage.service";

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Input() item: any;

  constructor(private storageService: LocalStorageService) {
  }

  ngOnInit(): void {
  }

  _check_stock(stockCount: any) {
    if (stockCount > 0) {
      return '#1890ff';
    } else {
      return '#000000';
    }
  }

  addToCart(item: any) {
    this.storageService.storeCartItems(item);
  }
}
