import {Component, OnInit} from '@angular/core';
import {ItemService} from "../../core/services/item.service";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  items: any;
  loading = false;
  array = [1, 2, 3, 4];

  constructor(private itemService: ItemService) {
  }

  ngOnInit(): void {
    this._all_items();
  }

  private _all_items() {
    this.itemService._all_items().subscribe(res => {
      this.items = res.elements;
      console.log(this.items);
      this.loading = false;
    });
  }
}
