import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {CustomerService} from "../../core/services/customer.service";
import {PaymentService} from "../../core/services/payment.service";
import {LocalStorageService} from "../../core/helpers/storage.service";
import {STORAGE_KEYS} from "../../core/helpers/STORAGE_KEYS";
import {Router} from "@angular/router";

interface ItemData {
  id: string;
  name: string;
  qty: number;
  price: number;
  sub: number;
}

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  email: any;
  phone: any;
  fname: any;
  lname: any;
  adl1: any;
  adl2: any;
  city: any;
  state: any;
  pcode: any;
  country: any;

  checked = false;
  sending_order = false;
  complete: any;

  constructor(private http: HttpClient, private customerService: CustomerService,
              private paymentService: PaymentService, private storage: LocalStorageService,private router: Router) {
  }

  ngOnInit(): void {
  }


  search_customer() {
    this.customerService._get_customer(this.email).subscribe(res => {
      console.log(res);
    });
  }

  placeOrder() {
    this.complete = 5;
    this.sending_order = true;
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'apikey': 'f46950c471ef47b89339e6aad9894358',
    }

    this.http.post<any>('https://try.readme.io/https://token-sandbox.dev.clover.com/v1/tokens',
      {
        "card": {
          "number": "6011361000006668",
          "exp_month": "12",
          "exp_year": "2021",
          "cvv": "123",
          "brand": "DISCOVER",
          "first6": "601136",
          "last4": "6668"
        }
      }, {headers}).subscribe(data => {
      this.complete = 25;
      this.create_customer(data);
    });
  }

  private create_customer(data: any) {
    this.complete = 27;
    const headers = {
      'Accept': '*/*',
      'Authorization': 'Bearer aefc3ccc-34fa-cb34-5a2c-63d0698082d8',
      'Content-Type': 'application/json',
    }
    this.http.post<any>('https://try.readme.io/https://scl-sandbox.dev.clover.com/v1/customers',
      {
        "ecomind": "ecom",
        "shipping": {
          "address": {
            "country": this.country,
            "line1": this.adl1,
            "city": this.city,
            "postal_code": this.pcode,
            "state": this.state
          }
        },
        "email": this.email,
        "firstName": this.fname,
        "lastName": this.lname,
        "name": this.fname + ' ' + this.lname,
        "phone": this.phone,
        "source": data.id
      }
      , {headers}).subscribe(data => {
      this.complete = 30;
      console.log(data);
      this.create_order(data);
    });
  }

  private create_order(data: any) {
    let items = [];
    for (let i = 0; i < this.storage.retrievesStoreItems(STORAGE_KEYS.CART_ITEMS).length; i++) {
      items.push({
        "tax_rates": [],
        "amount": this.storage.retrievesStoreItems(STORAGE_KEYS.CART_ITEMS)[i].price,
        "currency": "usd",
        "description": this.storage.retrievesStoreItems(STORAGE_KEYS.CART_ITEMS)[i].name,
        "parent": this.storage.retrievesStoreItems(STORAGE_KEYS.CART_ITEMS)[i].id,
        "quantity": this.storage.retrievesStoreItems(STORAGE_KEYS.CART_ITEMS)[i].qty,
        "type": "sku"
      })
    }
    this.complete = 50;
    const headers = {
      'Accept': '*/*',
      'Authorization': 'Bearer aefc3ccc-34fa-cb34-5a2c-63d0698082d8',
      'Content-Type': 'application/json',
    }
    this.http.post<any>('https://try.readme.io/https://scl-sandbox.dev.clover.com/v1/orders',
      {
        items,
        "shipping": {
          "address": {
            "city": this.city,
            "country": this.country,
            "line1": this.adl1,
            "postal_code": this.pcode,
            "state": this.state
          },
          "name": this.fname,
          "phone": this.phone
        },
        "currency": "usd",
        "customer": data.id,
        "email": this.email
      }
      , {headers}).subscribe(data => {
      this.complete = 75;
      console.log(data);
      this.make_payment(data);
    });
  }

  private make_payment(data: any) {
    this.complete = 80;
    const headers = {
      'Accept': '*/*',
      'Authorization': 'Bearer aefc3ccc-34fa-cb34-5a2c-63d0698082d8',
      'Content-Type': 'application/json',
    }
    this.http.post<any>('https://try.readme.io/https://scl-sandbox.dev.clover.com/v1/orders/' + data.id + '/pay',
      {
        "ecomind": "ecom",
        "customer": data.customer
      }
      , {headers}).subscribe(data => {
      this.complete = 100;
      console.log(data);
      this.sending_order = false;
      this.router.navigateByUrl('thank-you');
      this.storage.removeItem(STORAGE_KEYS.CART_ITEMS);
    });
  }
}
