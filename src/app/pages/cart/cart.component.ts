import {Component, OnInit} from '@angular/core';
import {LocalStorageService} from "../../core/helpers/storage.service";
import {STORAGE_KEYS} from "../../core/helpers/STORAGE_KEYS";
import {Router} from "@angular/router";

interface ItemData {
  id: string;
  name: string;
  qty: number;
  price: number;
  sub: number;
}

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})


export class CartComponent implements OnInit {
  public listOfData: ItemData[] = [];
  subTotal = '0.00';
  total = '0.00';
  tax = '2.00';
  shipping = '0.00';

  constructor(private storage: LocalStorageService, private router: Router) {
  }

  ngOnInit(): void {
    let item = undefined;
    const items = this.storage.retrievesStoreItems(STORAGE_KEYS.CART_ITEMS);
    for (let i = 0; i < items.length; i++) {
      item = this.alreadyExist(items[i]);
      if (item === undefined) {
        this.listOfData.push({
          id: items[i].id,
          name: items[i].name,
          qty: 1,
          price: items[i].price,
          sub: items[i].price
        });
      } else {
        item.qty = (item.qty + 1)
        item.sub = (item.qty * item.price);
        this.listOfData.splice(this.listOfData.indexOf(item), 1);
        this.listOfData.push(item);
      }
    }
    this.updateTotals();
  }

  private alreadyExist(item1: any) {
    for (let i = 0; i < this.listOfData.length; i++) {
      if (item1.id === this.listOfData[i].id) {
        return this.listOfData[i];
      }
    }
    return undefined;
  }

  removeQty(indexOfelement: number) {
    if (this.listOfData[indexOfelement].qty > 1) {
      this.listOfData[indexOfelement].qty = (this.listOfData[indexOfelement].qty - 1);
      this.listOfData[indexOfelement].sub = this.getSubTotal(this.listOfData[indexOfelement]);
      this.updateTotals();
    }
  }

  addQty(indexOfelement: number) {
    this.listOfData[indexOfelement].qty = (this.listOfData[indexOfelement].qty + 1);
    this.listOfData[indexOfelement].sub = this.getSubTotal(this.listOfData[indexOfelement]);
    this.updateTotals();
  }

  removeItem(item: any) {
    this.listOfData.splice(this.listOfData.indexOf(item), 1);
    this.updateTotals();
  }

  private updateTotals() {
    this.subTotal = '0.00';
    this.total = '0.00';

    let tot;
    for (let i = 0; i < this.listOfData.length; i++) {
      tot = (parseFloat(this.subTotal) + this.listOfData[i].sub);
      this.subTotal = parseFloat(tot + '').toFixed(2);
    }
    this.total = parseFloat(this.subTotal) + parseFloat(this.subTotal) * (2 / 100) + '';
  }

  private getSubTotal(itemDatum: ItemData) {
    return itemDatum.price * itemDatum.qty;
  }

  async checkout() {
    await this.storage.checkoutProducts(this.listOfData);
    await this.storage.storeOnLocalStorage('TOTAL', this.total);
    this.router.navigateByUrl('checkout');
  }
}
