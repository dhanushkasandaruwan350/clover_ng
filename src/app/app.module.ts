import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NZ_I18N} from 'ng-zorro-antd/i18n';
import {en_US} from 'ng-zorro-antd/i18n';
import {registerLocaleData} from '@angular/common';
import en from '@angular/common/locales/en';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {StorageServiceModule} from 'ngx-webstorage-service';
import {NzButtonModule} from "ng-zorro-antd/button";
import {TopNavComponent} from './navs/top-nav/top-nav.component';
import {NzMenuModule} from "ng-zorro-antd/menu";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzImageModule} from "ng-zorro-antd/image";
import {NzSpaceModule} from "ng-zorro-antd/space";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzAvatarModule} from "ng-zorro-antd/avatar";
import {NzTypographyModule} from "ng-zorro-antd/typography";
import {NzInputModule} from "ng-zorro-antd/input";
import {ProductCardComponent} from './components/product-card/product-card.component';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {authInterceptorProviders} from "./core/helpers/auth-intercepter.service";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzBadgeModule} from "ng-zorro-antd/badge";
import {NzSpinModule} from "ng-zorro-antd/spin";
import {NzCarouselModule} from "ng-zorro-antd/carousel";
import {LocalStorageService} from "./core/helpers/storage.service";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzDividerModule} from "ng-zorro-antd/divider";
import { CartComponent } from './pages/cart/cart.component';
import {NzTableModule} from "ng-zorro-antd/table";
import {NzPopconfirmModule} from "ng-zorro-antd/popconfirm";
import {NzEmptyModule} from "ng-zorro-antd/empty";
import {NzBreadCrumbModule} from "ng-zorro-antd/breadcrumb";
import { CheckoutComponent } from './pages/checkout/checkout.component';
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {NzProgressModule} from "ng-zorro-antd/progress";
import { ThankyouComponent } from './pages/thankyou/thankyou.component';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    TopNavComponent,
    ProductCardComponent,
    HomePageComponent,
    CartComponent,
    CheckoutComponent,
    ThankyouComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        StorageServiceModule,
        NzButtonModule,
        NzMenuModule,
        NzIconModule,
        NzImageModule,
        NzSpaceModule,
        NzGridModule,
        NzAvatarModule,
        NzTypographyModule,
        NzInputModule,
        NzCardModule,
        NzBadgeModule,
        NzSpinModule,
        NzCarouselModule,
        NzLayoutModule,
        NzDividerModule,
        NzTableModule,
        NzPopconfirmModule,
        NzEmptyModule,
        NzBreadCrumbModule,
        NzSelectModule,
        NzFormModule,
        NzCheckboxModule,
        ReactiveFormsModule,
        NzDatePickerModule,
        NzProgressModule
    ],
  providers: [{provide: NZ_I18N, useValue: en_US}, authInterceptorProviders, LocalStorageService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
